/**
 * Created by jkass on 11/28/2016.
 *
 * Count Words in a String – Counts the number of individual words in a string.
 * For added complexity read these strings in from a text file and generate a summary.
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CountWords {

    public static int countWords (String filePath, String delim){
        //String myFile = "../testFile.txt";
        String myFile = filePath;
        int wordCount = 0;
        int totalWordCount = 0;
        BufferedReader br = null;
        String[] numWords;
        int lineNumber = 1;

    try{

        String line;
        br = new BufferedReader(new FileReader(myFile));
        while((line = br.readLine()) != null) {
            numWords = line.split(delim);
            wordCount = numWords.length;
            System.out.println("Number of words in line " + lineNumber + " is:" + wordCount);
            lineNumber++;
            totalWordCount += wordCount;
            wordCount = 0;
        }

    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            if (br != null)br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

        return totalWordCount;
    }


    public static void main (String[] args){

        String myFilePath = "C:\\Users\\jkass\\Dropbox\\Personal\\programming\\Learning\\Jeff\\countwords\\CountWords\\testFile.txt";

        int result = countWords(myFilePath, " ");

        System.out.println("Number of words in the file is: " + result);

    }


}
